FROM golang:1.19-alpine3.17

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o ./go-mongo-app

EXPOSE 3000

CMD ["./go-mongo-app"]
